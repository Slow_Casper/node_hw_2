enum CurrencyEnum {
    USD = 'USD',
    UAH = 'UAH'
}

export default CurrencyEnum;