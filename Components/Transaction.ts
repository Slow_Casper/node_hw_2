import { randomUUID } from 'crypto';
import CurrencyEnum from './CurrensyEnum';

class Transaction {
    id: string;
    constructor(public amount: number, public currency: CurrencyEnum) {
        this.id = randomUUID();
    }
}

export default Transaction;
