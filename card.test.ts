import Card from "./Components/Card";
import CurrencyEnum from "./Components/CurrensyEnum";
import Transaction from "./Components/Transaction";

describe('Card', () => {
    let card;

    beforeEach(() => {
        card = new Card();
    });
    
    it('Add transaction case', () => {
        const transaction = new Transaction(55, CurrencyEnum.UAH);
        const id = card.addTransaction(transaction);
        expect(id).toBe(transaction.id)
    });

    it('Get transaction by id case', () => {
        const transaction = new Transaction(200, CurrencyEnum.UAH);
        const id = card.addTransaction(transaction);
        const currentTransaction = card.getTransaction(id);
        expect(currentTransaction).toEqual(transaction);
    });

    it('Get balance by currency case', () => {
        card.addTransaction(CurrencyEnum.UAH, 200);
        card.addTransaction(CurrencyEnum.USD, 200);
        card.addTransaction(CurrencyEnum.UAH, 500);

        const uah = card.getBalance(CurrencyEnum.UAH);
        const usd = card.getBalance(CurrencyEnum.USD);

        expect(uah).toEqual(700);
        expect(usd).toEqual(200);
    })
});