import Card from "./Components/Card";
import CurrencyEnum from "./Components/CurrensyEnum";

const card = new Card();

const transactionId1 = card.addTransaction(CurrencyEnum.USD, 50);
const transactionId2 = card.addTransaction(CurrencyEnum.UAH, 100);

console.log('Transaction 1:', card.getTransaction(transactionId1));
console.log('Transaction 2:', card.getTransaction(transactionId2));